package com.example.foobar.controller;

import com.example.foobar.entity.User;
import com.example.foobar.repositories.UserRepository;
import com.example.foobar.service.UserService;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/users")
public class UserController {
  @Autowired private UserRepository userRepository;

  @Autowired private UserService userService;

  @GetMapping
  public ResponseEntity<List<User>> getAllUsers() {
    return userService.getAllUsers();
  }

  @PostMapping
  public ResponseEntity<User> saveUser(@Valid @RequestBody User user) {
    return userService.saveUser(user);
  }

  @PutMapping(value = "/{id}")
  public ResponseEntity<User> updateUser(@PathVariable Long id, @Valid @RequestBody User user) {
    return userService.updateUser(id, user);
  }

  @DeleteMapping("/{id}")
  public void deleteUser(@PathVariable Long id) {
    userService.deleteUser(id);
  }

  @GetMapping("/{id}")
  public ResponseEntity<User> getUser(@PathVariable Long id) {
    return userService.getUserById(id);
  }
}
