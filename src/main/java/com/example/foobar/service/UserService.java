package com.example.foobar.service;

import com.example.foobar.entity.User;
import com.example.foobar.exceptions.RecordNotFoundException;
import com.example.foobar.repositories.UserRepository;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class UserService {
  private UserRepository userRepository;

  @Autowired
  public UserService(UserRepository userRepository) {
    this.userRepository = userRepository;
  }

  public ResponseEntity<User> saveUser(User user) {
    User savedUser = userRepository.save(user);
    return new ResponseEntity<User>(savedUser, HttpStatus.CREATED);
  }

  public ResponseEntity<List<User>> getAllUsers() {
    List users = userRepository.findAll();
    return new ResponseEntity<>(users, HttpStatus.OK);
  }

  public ResponseEntity<User> getUserById(Long id) {
    User user = findExistingUserById(id);
    return new ResponseEntity<User>(user, HttpStatus.OK);
  }

  public ResponseEntity<User> updateUser(Long id, User apiUser) {
    User user = findExistingUserById(id);
    // Todo: inefficient, use an update db query
    if (user != null) {
      user.setFirstName(apiUser.getFirstName());
      user.setFirstName(apiUser.getFirstName());
      user.setSalary(apiUser.getSalary());
    }
    userRepository.save(user);
    return new ResponseEntity<User>(user, HttpStatus.OK);
  }

  public ResponseEntity<User> deleteUser(Long id) {
    User user = findExistingUserById(id);
    userRepository.deleteById(user.getId());
    return new ResponseEntity<User>(HttpStatus.NO_CONTENT);
  }

  private User findExistingUserById(Long id) {
    Optional<User> user = userRepository.findById(id);

    if (user.equals(Optional.empty())) {
      throw new RecordNotFoundException();
    }
    return user.get();
  }
}
