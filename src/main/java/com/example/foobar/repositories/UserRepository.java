package com.example.foobar.repositories;

import com.example.foobar.entity.User;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface UserRepository extends JpaRepository<User, Long> {
  @Query("SELECT u FROM User u WHERE u.email = ?1")
  Optional<User> findUserByEmail(String email);

  //  @Modifying
  //  @Query("update User u set u.firstname = ?1, u.lastname = ?2 u.email = ?3, where u.id = ?3")
  //  void setUserInfoById(String firstname, String lastname, Integer userId);
}
