
Create the db
Update application.properties with db credentials
Update flyway.conf

##Run flyway migrations
```
mvn clean flyway:migrate -Dflyway.configFiles=flyway.conf
```
